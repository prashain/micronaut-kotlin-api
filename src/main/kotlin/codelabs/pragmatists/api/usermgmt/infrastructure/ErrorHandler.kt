package codelabs.pragmatists.api.usermgmt.infrastructure

import io.micronaut.context.annotation.Requires
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Produces
import io.micronaut.http.server.exceptions.ExceptionHandler
import javax.inject.Singleton

@Produces
@Singleton
@Requires(classes = [UserNotfoundException::class, ExceptionHandler::class])
open class ErrorHandler() : ExceptionHandler<UserNotfoundException, HttpResponse<ErrorModel>> {

    override open fun handle(
            req: HttpRequest<Any>,
            exception: UserNotfoundException
    ): HttpResponse<ErrorModel> {

        val errorModel1 = ErrorModel(Code.RED, "Inforrect ", "fasfsdfsad")
        return HttpResponse.badRequest(errorModel1)
    }
}
