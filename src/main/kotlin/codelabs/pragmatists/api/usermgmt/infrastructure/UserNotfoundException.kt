package codelabs.pragmatists.api.usermgmt.infrastructure

open class UserNotfoundException: RuntimeException{

    constructor(ctx: Exception) : super(ctx)

    constructor(message:String,ctx: Exception) : super(message,ctx)

    constructor(message:String) : super(message)

}