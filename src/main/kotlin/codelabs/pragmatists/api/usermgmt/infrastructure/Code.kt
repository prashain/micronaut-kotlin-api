package codelabs.pragmatists.api.usermgmt.infrastructure


enum class Code (val code:Int, val description:String) {
    RED(14567,"Incorrect data supplied"), 
    AMBER(14568,"Incorrect data supplied"), 
    GREEN(14569,"Incorrect data supplied"), 
}