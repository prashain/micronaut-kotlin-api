package codelabs.pragmatists.api.usermgmt.infrastructure

import io.micronaut.core.annotation.Introspected

@Introspected
data class ErrorModel(val code:Code?,val message:String?,val description:String?)
