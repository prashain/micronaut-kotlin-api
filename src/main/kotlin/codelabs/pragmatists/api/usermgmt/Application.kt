package codelabs.pragmatists.api.usermgmt

import io.micronaut.runtime.Micronaut.*
fun main(args: Array<String>) {
	build()
	    .args(*args)
		.packages("codelabs.pragmatists.api")
		.start()
}

