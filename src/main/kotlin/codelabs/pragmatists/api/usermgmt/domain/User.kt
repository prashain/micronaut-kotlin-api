package codelabs.pragmatists.api.usermgmt.domain

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.NotBlank
import io.micronaut.core.annotation.Introspected

@Entity
 @Introspected data class User(
    @NotNull @Id  @Column(name="user_id") val userId:Int,
    @NotNull @NotBlank @Column(name="username")val username:String, 
    @NotNull @NotBlank @Column(name="first_name") val firstName:String, 
    @NotNull @NotBlank @Column(name="last_name")  val lastName:String,
    @NotNull @NotBlank val email:String
    )