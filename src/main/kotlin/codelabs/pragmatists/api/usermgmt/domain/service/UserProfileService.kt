package codelabs.pragmatists.api.usermgmt.domain.service

import codelabs.pragmatists.api.usermgmt.domain.*
import codelabs.pragmatists.api.usermgmt.domain.service.*
import codelabs.pragmatists.api.usermgmt.infrastructure.UserNotfoundException
import javax.inject.Singleton
import javax.inject.Inject
import javax.validation.Valid

@Singleton
open class UserProfileService(@Inject val repo : UserProfileRepository) {
    
    fun findUserProfileByUserId(usedId: Int): User {
        val user = User(usedId, "username", "firstName", "lastName","prayadav@gmail.com")
        return repo.findById(usedId).orElseThrow( {throw UserNotfoundException("ASfasdf")})
    }

    open fun addUser(@Valid user: User) {
         repo.save(user);
    }

    open fun deleteUser(usedId: Int):Unit {
        repo.deleteById(usedId);
    }


    fun findAllUsers() : Iterable<User>{
        return repo.findAll();
    }
}
