package codelabs.pragmatists.api.usermgmt.domain.service
import codelabs.pragmatists.api.usermgmt.domain.*
import io.micronaut.data.repository.CrudRepository
import io.micronaut.data.annotation.Repository

@Repository
interface UserProfileRepository : CrudRepository<User,Int>{
    
}