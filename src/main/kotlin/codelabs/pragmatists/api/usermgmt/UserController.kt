package codelabs.pragmatists.api.usermgmt

import codelabs.pragmatists.api.usermgmt.domain.*
import codelabs.pragmatists.api.usermgmt.domain.service.UserProfileService
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.fge.jsonschema.main.JsonSchemaFactory
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MutableHttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.cookie.Cookie
import io.micronaut.http.simple.cookies.SimpleCookie
import io.micronaut.validation.Validated
import java.net.URI
import javax.inject.Inject

@Controller("/userapi/v1")
@Validated
open class UserController( val userSvc: UserProfileService) {

    val mapper = ObjectMapper().registerKotlinModule()

    @Get(uri = "/users/{id}")
    open fun index(@PathVariable id: Int, req:HttpRequest<Any>): MutableHttpResponse<Any>? {
        println(req.headers.asMap())

        return HttpResponse.redirect<Any>((URI.create("http://test.ia.group.codelabs.com:9000/test"))).cookie(SimpleCookie("Set-Cookie","CTSession=2134new;correlation-id=45;path=/;httpOnly=true;domain=.codelabs.com"))
        //header("Set-Cookie","CTSession=2134;correlation-id=45;path=/;httpOnly=true;domain=.codelabs.com")
       // return HttpResponse.ok(userSvc.findUserProfileByUserId(id)).header("Set-Cookie","CTSession=2134;correlation-id=45;path=/;httpOnly=true;domain=.codelabs.com")

    }

    @Post(uri = "/users/{id}")
    open fun index1(@PathVariable id: Int, req:HttpRequest<Any>) {
        println(req.headers.asMap())
        // return HttpResponse.ok(userSvc.findUserProfileByUserId(id)).header("Set-Cookie","CTSession=2134;correlation-id=45;path=/;httpOnly=true;domain=.codelabs.com")

    }

    @Get(uri = "/users", produces = ["application/json"])
    open fun users(): Iterable<User> {
        return userSvc.findAllUsers()
    }
    @Post(uri = "/user", consumes = ["application/json"])
    open fun index(@Body user: String): HttpResponse<String> {
        if (validateSchema(user)) {
            val parsedUser: User = mapper.readValue(user)
            userSvc.addUser(parsedUser)
            return HttpResponse.created(
                    "{\"message\": \"User with ${parsedUser.userId} created \"}")
        } else {
            return HttpResponse.badRequest("{\"message\": \"The request body is invalid\"}")
        }
    }

    @Delete(uri = "/users/{userId}")
    open fun deleteUser(@PathVariable userId: Int): HttpResponse<String> {

        userSvc.deleteUser(userId)
        return HttpResponse.noContent()
    }

    
    fun validateSchema(user: String): Boolean {
        val fileContent = this::class.java.getResource("/schema/user.json").readText()
        val objectMapper: ObjectMapper = ObjectMapper()
        val jsonNode = objectMapper.readTree(fileContent)

        val factory = JsonSchemaFactory.byDefault()
        val schema = factory.getJsonSchema(jsonNode)

        println(schema.validate(objectMapper.readTree(user)))

        return schema.validate(objectMapper.readTree(user)).isSuccess
    }
}
