* To run the api
-   mvn clean spring-boot:run
-  To run the application
```
  curl --request GET \
  --url http://localhost:8080/stream \
  --header 'Accept: application/x-ndjson' \
  --header 'X-Brand: BOS' \
  --header 'X-lbgClientID: sfsdf' \
  --header 'X-lbgTransactionID: asfasfasdfa' \
  --header 'x-lbg-env: cbo' \
  --cookie JSESSIONID=0000fNqVb5a5P8LKfLcE8nJ5XGX%3Ab0c2c64e-9917-4b58-9138-91670f8a2308
  ```
